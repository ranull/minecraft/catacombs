package net.steeleyes.catacombs;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;

public class BlockChange {
    private Block blk;
    private Material mat;
    private BlockFace face;
    private EntityType spawner;
    private String[] line;

    public BlockChange(Block blk, Material mat) {
        this.spawner = null;
        this.line = null;
        this.blk = blk;
        this.mat = mat;
    }

    public BlockChange(Block blk, Material mat, BlockFace face) {
        this.spawner = null;
        this.line = null;
        this.blk = blk;
        this.mat = mat;
        this.face = face;
    }

    public Block getBlk() {
        return this.blk;
    }

    public BlockFace getFace() {
        return this.face;
    }

    public Material getMat() {
        return this.mat;
    }

    public EntityType getSpawner() {
        return this.spawner;
    }

    public void setSpawner(EntityType spawner) {
        this.spawner = spawner;
    }

    public Boolean hasLines() {
        return Boolean.valueOf((this.line != null));
    }

    public void setLine(int i, String str) {
        if (this.line == null) {
            this.line = new String[4];
        }
        if (i >= 0 && i <= 3) {
            this.line[i] = str;
        }
    }

    public String getLine(int i) {
        if (this.line != null && i >= 0 && i <= 3) {
            return this.line[i];
        }
        return null;
    }
}
