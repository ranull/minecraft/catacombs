package net.steeleyes.catacombs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

public class CatListener implements Listener {
    private Catacombs plugin;

    public CatListener(final Catacombs plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onBlockDamage(BlockDamageEvent event) {
        // TODO FIX FIRING TWICE
        Block blk = event.getBlock();
        CatUtils.toggleSecretDoor(blk);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerRespawn(PlayerRespawnEvent evt) {
        Player player = evt.getPlayer();
        if (plugin.getPlayers().hasGear(player))
            if (plugin.getCnf().DeathKeepGear() &&
                    CatUtils.takeCash(player, plugin.getCnf().DeathGearCost(), "to restore your equipment"))
                plugin.getPlayers().restoreGear(player);
            else plugin.getPlayers().dropGear(player);

        Location loc = plugin.getPlayers().getRespawn(player);
        if (loc != null) {
            evt.setRespawnLocation(loc);
            player.sendMessage("Respawning in the hut");
            plugin.getPlayers().setRespawn(player, null);
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onCreatureSpawn(CreatureSpawnEvent evt) {
        Block blk = evt.getLocation().getBlock();
        if (evt.getEntity().getWorld().getEnvironment() != World.Environment.THE_END && evt.getEntity() instanceof Monster &&
                ((plugin.getCnf().MobsSpawnOnlyUnderground() && evt.getSpawnReason() == SpawnReason.NATURAL && CatUtils.onSurface(blk))
                        || (plugin.getDungeons().getDungeon(blk) == null && plugin.getCnf().MobsSpawnOnlyInDungeons())))
            evt.setCancelled(true);
    }


    @EventHandler(priority = EventPriority.LOW)
    public void onWorldLoad(WorldLoadEvent evt) {
        int cnt = plugin.loadWorld(evt.getWorld().getName());
        System.out.println("[Catacombs] Dynamic world load " + evt.getWorld().getName() + " " + cnt + " dungeon(s)");
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onWorldUnload(WorldUnloadEvent evt) {
        System.out.println("[Catacombs] Dynamic world unload " + evt.getWorld().getName());
        plugin.unloadWorld(evt.getWorld().getName());
    }
}
