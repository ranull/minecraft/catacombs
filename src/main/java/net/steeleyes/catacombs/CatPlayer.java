package net.steeleyes.catacombs;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class CatPlayer {
    Player player;
    Location respawn = null;

    public CatPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public Location getRespawn() {
        return respawn;
    }

    public void setRespawn(Location respawn) {
        this.respawn = respawn;
    }
}
