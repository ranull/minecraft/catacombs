package net.steeleyes.catacombs;

public class Vector {
    public int x, y, z;

    public Vector(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "Vector(" + x + "," + y + "," + z + ")";
    }
}
