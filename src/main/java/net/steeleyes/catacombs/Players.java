package net.steeleyes.catacombs;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class Players {
    public final Map<Player, CatPlayer> players = new HashMap<>();

    public final Map<Player, CatGear> gear = new HashMap<>();

    public Boolean hasGear(Player player) {
        return gear.containsKey(player);
    }

    public void saveGear(Player player) {
        CatGear stuff = new CatGear(player);
        gear.put(player, stuff);
    }

    public void dropGear(Player player) {
        if (gear.containsKey(player)) {
            CatGear stuff = gear.get(player);
            stuff.dropGear();
        }
    }

    public void restoreGear(Player player) {
        if (gear.containsKey(player)) {
            CatGear stuff = gear.get(player);
            stuff.restoreGear();
            gear.remove(player);
        }
    }

    public void setRespawn(Player player, Location loc) {
        if (players.containsKey(player)) {
            CatPlayer cp = players.get(player);
            cp.setRespawn(loc);
        } else {
            CatPlayer cp = new CatPlayer(player);
            players.put(player, cp);
            cp.setRespawn(loc);
        }
    }

    public Location getRespawn(Player player) {
        if (players.containsKey(player)) {
            return players.get(player).getRespawn();
        }
        return null;
    }

    public void add(CatPlayer cp) {
        players.put(cp.getPlayer(), cp);
    }

    public CatPlayer get(Player player) {
        return players.get(player);
    }
}
