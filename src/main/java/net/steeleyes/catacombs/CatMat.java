package net.steeleyes.catacombs;

import org.bukkit.Material;
import org.bukkit.block.Block;

@SuppressWarnings("deprecation")
public class CatMat {
    private Material mat;

    public CatMat(Material mat) {
        this.mat = mat;
    }

    public CatMat(Block blk) {
        this.mat = blk.getType();
    }

    public static CatMat parseMaterial(String string) {
        String name = string;
        if (string.contains(":")) {
            String[] part = name.split(":");
            name = part[0];
        }
        Material mat = Material.matchMaterial(name);
        if (mat == null || !mat.isBlock()) {
            System.err.println("[Catacombs] Invalid block material '" + string + "'");
            return null;
        }
        return new CatMat(mat);
    }

    public String toString() {
        return this.mat.toString();
    }

    public Boolean equals(CatMat that) {
        if (that == null) {
            return false;
        }
        return this.mat.equals(that.mat);
    }

    public Boolean equals(Block blk) {
        return this.mat.equals(blk.getType());
    }

    public Boolean is(Material that) {
        return this.mat.equals(that);
    }


    public void setBlock(Block blk) {
        blk.setType(this.mat);
    }

    public void getBlock(Block blk) {
        Material m = blk.getType();
        this.mat = m;
    }

    public void get(CatMat that) {
        this.mat = that.mat;
    }

    public Material getMat() {
        return this.mat;
    }
}
