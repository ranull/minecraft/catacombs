package net.steeleyes.catacombs;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CatLoot {
    public static void fillChest(Inventory inv, List<String> list) {
        for (ItemStack i : fillChest(new ArrayList<>(), list)) inv.addItem(i);
    }

    public static List<ItemStack> fillChest(List<ItemStack> inv, List<String> list) {
        if (inv == null) inv = new ArrayList<>();
        for (String s : list) {
            ItemStack i = fromString(s);
            if (i != null) inv.add(i);
        }
        return inv;
    }

    public static ItemStack fromString(String string) {
        try {
            return generateItem(string);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // Create item from config string
    public static ItemStack generateItem(String string) {
        String[] part = string.split(" ");
        //Load type and amount
        String[] main = part[0].split(":");

        // Chance
        Integer chance = 100;
        try {
            chance = Integer.parseInt(main[1]);
        } catch (Exception e) {
            System.out.println("[Warning] Invalid chance whilst loading lootList! (" + main[1] + ")");
            return null;
        }

        if (new Random().nextInt(100) >= chance) {
            return null;
        }

        int amount = 1;
        try {
            String[] amountArray = main[2].replace(" ", "").split("-");
            if (amountArray.length == 1) {
                amount = Integer.parseInt(amountArray[0]);
            }
            else {
                int max = Integer.parseInt(amountArray[1]);
                int min = Integer.parseInt(amountArray[0]);
                amount = new Random().nextInt(max - min) + min;
            }
        } catch (Exception e) {
        }

        // Type
        Material material = Material.matchMaterial(main[0].toUpperCase());
        if (material == null) {
            System.out.println("[Warning] Invalid material-type whilst loading lootList! (" + main[0] + ")");
            return null;
        }

        ItemStack item = new ItemStack(material, amount);

        // TODO EXTRA INFO
        //Load and apply params
        /*
        if (sa.length > 2) {
            ItemMeta im = i.getItemMeta();
            for (int n = 2; n < sa.length; n++) {
                String[] pa = sa[n].split(":");
                if (pa.length < 2) continue;
                if (pa[0].equalsIgnoreCase("name")) {
                    im.setDisplayName(ChatColor.translateAlternateColorCodes('&', pa[1].replaceAll("_", " ")));
                } else if (pa[0].equalsIgnoreCase("lore")) {
                    List<String> lore = im.getLore();
                    if (lore == null) lore = new ArrayList<>();
                    for (String s : pa[1].split("[|]"))
                        if (s != null) lore.add(ChatColor.translateAlternateColorCodes('&', s.replaceAll("_", " ")));
                    im.setLore(lore);
                } else {
                    Enchantment e;
                    try {
                        //e = Enchantment.getById(Integer.parseInt(pa[0]));
                        e = Enchantment.DURABILITY; // TODO GET ENCHANT
                    } catch (Exception ignored) {
                        e = Enchantment.getByName(pa[0]);
                    }
                    if (e != null) {
                        try {
                            Integer lvl = Integer.parseInt(pa[1]);
                            im.addEnchant(e, lvl, true);
                        } catch (NumberFormatException ignored) {
                        }
                    } else for (PotionType ptv : PotionType.values())
                        if (ptv.name().equalsIgnoreCase(pa[0])) try {
                            Integer lvl = Integer.parseInt(pa[1]);
                            if (lvl > 2) lvl = 2;
                            if (lvl < 1) lvl = 1;
                            new Potion(ptv, lvl).apply(i);
                            break;
                        } catch (Exception ignored) {
                        }
                }
            }
            i.setItemMeta(im);
        }
         */
        return item;
    }

    public static void smallChest(CatConfig cnf, Inventory inv) {
        if (cnf.SmallEquipChance()) {
            inv.addItem(leather_equipment(cnf));
        }
        fillChest(inv, cnf.LootSmallList());
    }

    public static List<ItemStack> smallChest(CatConfig cnf, List<ItemStack> inv) {
        if (cnf.SmallEquipChance()) {
            inv.add(leather_equipment(cnf));
        }
        return fillChest(inv, cnf.LootBigList());
    }

    public static void midChest(CatConfig cnf, Inventory inv) {
        if (cnf.MedEquipChance()) if (cnf.Chance(90)) {
            inv.addItem(new ItemStack(Material.IRON_INGOT, cnf.nextInt(10) + 1));
            inv.addItem(iron_equipment(cnf));
        } else {
            inv.addItem(new ItemStack(Material.GOLD_INGOT, cnf.nextInt(10) + 1));
            inv.addItem(gold_equipment(cnf));
            inv.addItem(new ItemStack(Material.GOLDEN_APPLE, 1));
        }
        fillChest(inv, cnf.LootMediumList());
        if (cnf.MedEquipChance()) smallChest(cnf, inv);
    }

    public static List<ItemStack> midChest(CatConfig cnf, List<ItemStack> inv) {
        if (cnf.MedEquipChance()) if (cnf.Chance(90)) {
            inv.add(new ItemStack(Material.IRON_INGOT, cnf.nextInt(10) + 1));
            inv.add(iron_equipment(cnf));
        } else {
            inv.add(new ItemStack(Material.GOLD_INGOT, cnf.nextInt(10) + 1));
            inv.add(gold_equipment(cnf));
            inv.add(new ItemStack(Material.GOLDEN_APPLE, 1));
        }
        inv = fillChest(inv, cnf.LootMediumList());
        if (cnf.MedSmallChance()) inv = smallChest(cnf, inv);
        return inv;
    }

    public static void bigChest(CatConfig cnf, Inventory inv) {
        if (cnf.BigEquipChance()) {
            inv.addItem(diamond_equipment(cnf));
        }
        fillChest(inv, cnf.LootBigList());
        if (cnf.BigSmallChance()) {
            smallChest(cnf, inv);
        }
    }

    public static List<ItemStack> bigChest(CatConfig cnf, List<ItemStack> inv) {
        if (cnf.BigEquipChance()) {
            inv.add(diamond_equipment(cnf));
        }
        inv = fillChest(inv, cnf.LootBigList());
        if (cnf.BigSmallChance()) {
            inv = smallChest(cnf, inv);
        }
        return inv;
    }

    private static ItemStack leather_equipment(CatConfig cnf) {
        switch (cnf.nextInt(6) + 1) {
            case 1:
                return new ItemStack(Material.LEATHER_HELMET, 1);
            case 2:
                return new ItemStack(Material.LEATHER_CHESTPLATE, 1);
            case 3:
                return new ItemStack(Material.LEATHER_LEGGINGS, 1);
            case 4:
                return new ItemStack(Material.LEATHER_BOOTS, 1);
        }
        return new ItemStack(Material.AIR, 1);
    }

    private static ItemStack iron_equipment(CatConfig cnf) {
        switch (cnf.nextInt(12) + 1) {
            case 1:
                return new ItemStack(Material.IRON_HELMET, 1);
            case 2:
                return new ItemStack(Material.IRON_CHESTPLATE, 1);
            case 3:
                return new ItemStack(Material.IRON_LEGGINGS, 1);
            case 4:
                return new ItemStack(Material.IRON_BOOTS, 1);
            case 5:
                return new ItemStack(Material.IRON_PICKAXE, 1);
            case 6:
                return new ItemStack(Material.IRON_SHOVEL, 1);
            case 7:
                return new ItemStack(Material.IRON_AXE, 1);
            case 8:
                return new ItemStack(Material.IRON_SWORD, 1);
            case 9:
                return new ItemStack(Material.IRON_HOE, 1);
        }
        return new ItemStack(Material.AIR, 1);
    }

    private static ItemStack gold_equipment(CatConfig cnf) {
        switch (cnf.nextInt(12) + 1) {
            case 1:
                return new ItemStack(Material.GOLDEN_HELMET, 1);
            case 2:
                return new ItemStack(Material.GOLDEN_CHESTPLATE, 1);
            case 3:
                return new ItemStack(Material.GOLDEN_LEGGINGS, 1);
            case 4:
                return new ItemStack(Material.GOLDEN_BOOTS, 1);
            case 5:
                return new ItemStack(Material.GOLDEN_PICKAXE, 1);
            case 6:
                return new ItemStack(Material.GOLDEN_SHOVEL, 1);
            case 7:
                return new ItemStack(Material.GOLDEN_AXE, 1);
            case 8:
                return new ItemStack(Material.GOLDEN_SWORD, 1);
            case 9:
                return new ItemStack(Material.GOLDEN_HOE, 1);
        }
        return new ItemStack(Material.AIR, 1);
    }

    private static ItemStack diamond_equipment(CatConfig cnf) {
        switch (cnf.nextInt(12) + 1) {
            case 1:
                return new ItemStack(Material.DIAMOND_HELMET, 1);
            case 2:
                return new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
            case 3:
                return new ItemStack(Material.DIAMOND_LEGGINGS, 1);
            case 4:
                return new ItemStack(Material.DIAMOND_BOOTS, 1);
            case 5:
                return new ItemStack(Material.DIAMOND_PICKAXE, 1);
            case 6:
                return new ItemStack(Material.DIAMOND_SHOVEL, 1);
            case 7:
                return new ItemStack(Material.DIAMOND_AXE, 1);
            case 8:
                return new ItemStack(Material.DIAMOND_SWORD, 1);
            case 9:
                return new ItemStack(Material.DIAMOND_HOE, 1);
        }
        return new ItemStack(Material.AIR, 1);
    }
}
