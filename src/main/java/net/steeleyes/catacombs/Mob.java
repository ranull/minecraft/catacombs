package net.steeleyes.catacombs;

import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent;

public class Mob {
    private LivingEntity ent;
    private MobType type;
    private int hps;
    Boolean notify = false;

    public Mob(MobType type, Location loc) {
        this.type = type;
        ent = type.spawn(loc);
        hps = type.getHps();
    }

    public LivingEntity getEnt() {
        return ent;
    }

    public MobType getType() {
        return type;
    }

    public Boolean getNotify() {
        return notify;
    }

    public void damage(EntityDamageEvent evt) {
        hps = ((Double) (hps - evt.getDamage())).intValue();
        evt.setDamage(1);
        if (hps <= 0) ent.setHealth(1);
        else ent.setHealth(ent.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());
    }
}
