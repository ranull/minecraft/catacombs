package net.steeleyes.catacombs;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import net.steeleyes.data.CatSQL;
import net.steeleyes.maps.Direction;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class Catacombs extends JavaPlugin {
    private CatConfig cnf;
    private Dungeons dungeons;
    private CatSQL sql = null;
    private BlockChangeHandler handler;

    private Players players = new Players();
    private MobTypes mobtypes = null;

    private PluginDescriptionFile info;
    private Boolean enabled = false;

    private File mapdir;

    private static Permission permission = null;
    private static Economy economy = null;

    private final CatListener listener = new CatListener(this);

    @Override
    public void onLoad() {
        cnf = new CatConfig(getConfig());
        info = this.getDescription();

        mapdir = new File("plugins" + File.separator + info.getName() + File.separator + "maps");
        if (!mapdir.exists()) {
            mapdir.mkdir();
        }
    }

    @Override
    public void onEnable() {
        if (!enabled) {
            setupEconomy();
            setupDatabase();

            dungeons = new Dungeons(this, sql);

            PluginManager pm = this.getServer().getPluginManager();
            pm.registerEvents(listener, this);

            handler = new BlockChangeHandler(this);
            this.getServer().getScheduler().scheduleSyncRepeatingTask(this, handler, 40, 20);

            this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
                @Override
                public void run() {
                    for (Dungeon dung : getDungeons().all()) {
                        dung.maintain();
                    }
                }
            }, 40, 20);

            dungeons.clearMonsters(this);
            enabled = true;
        }
    }

    @Override
    public void onDisable() {
        enabled = false;
    }

    private void setupDatabase() {
        sql = new CatSQL("plugins" + File.separator + "Catacombs" + File.separator + "Catacombs.db");

        Boolean hasLevels = sql.tableExists("levels");
        Boolean hasDungeons = sql.tableExists("dungeons");
        sql.createTables();
        if (hasLevels && !hasDungeons) {
            System.out.println("[Catacombs] Converting old dungeon data to new format (slow)");
            sql.Convert2(this);
        }
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            System.out.println("[Catacombs] Vault not found, no cash will be given for kills");
            return false;
        }
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
            if (economy != null) {
                System.out.println("[Catacombs] Found economy system '" + economy.getName() + "'");
            }
        }
        return (economy != null);
    }

    public Boolean hasPermission(Player player, String perm) {
        if (player == null) return true;
        else if (permission != null) return permission.has(player, "catacombs.admin") || permission.has(player, perm);
        else return player.isOp();
    }

    public CatSQL getSql() {
        return sql;
    }

    public Dungeons getDungeons() {
        return dungeons;
    }

    public Players getPlayers() {
        return players;
    }

    public PluginDescriptionFile getInfo() {
        return info;
    }

    public static Economy getEconomy() {
        return economy;
    }

    public CatConfig getCnf() {
        return cnf;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            return Commands(player, args);
        }
        return Commands(null, args);
    }

    public Boolean Commands(Player p, String[] args) {
        try {
            if (args.length < 1) {
                help(p);
                // PLAN ********************************************************
            } else if (cmd(p, args, "plan", "dn")) {
                int depth = Integer.parseInt(args[2]);
                planDungeon(p, args[1], depth);
            } else if (cmd(p, args, "plan", "dnn")) {
                int depth = Integer.parseInt(args[2]);
                int radius = Integer.parseInt(args[3]);
                planDungeon(p, args[1], depth, radius);

                // SCATTER ********************************************************
            } else if (cmd(p, args, "scatter", "dnnn")) {
                int depth = Integer.parseInt(args[2]);
                int radius = Integer.parseInt(args[3]);
                int dist = Integer.parseInt(args[4]);
                scatterDungeon(p, args[1], depth, radius, dist);

                // UNPLAN ********************************************************
            } else if (cmd(p, args, "unplan", "u")) {
                if (dungeons.exists(args[1])) {
                    dungeons.remove(args[1]);
                    inform(p,"'" + args[1] + "' unplanned");
                }

                // BUILD ********************************************************
            } else if (cmd(p, args, "build", "p")) {
                buildDungeon(p, args[1]);

                // LIST ********************************************************
            } else if (cmd(p, args, "list")) {
                inform(p, "Catacombs: " + dungeons.getNames());
            } else if (cmd(p, args, "list", "D")) {
                List<String> i = dungeons.getinfo(args[1]);
                for (String s : i) {
                    System.out.println(s);
                }
                //inform(p,"Catacombs: "+dungeons.getNames());

            } else if (cmd(p, args, "dump", "D")) {
                File f = new File("plugins" + File.separator + "Catacombs", "dmp_" + args[1] + ".txt");
                FileWriter fstream = new FileWriter(f);
                BufferedWriter out = new BufferedWriter(fstream);
                for (String s : dungeons.dump(args[1])) {
                    out.write(s + "\r\n");
                }
                out.close();

                // MAP ********************************************************
            } else if (cmd(p, args, "map")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    for (String s : dung.map()) {
                        System.out.println(s);
                    }
                }
            } else if (cmd(p, args, "map", "D")) {
                for (String s : dungeons.map(args[1])) {
                    System.out.println(s);
                }

                // GOLD ********************************************************
            } else if (cmd(p, args, "gold")) {
                if (p != null) {
                    if (economy != null) {
                        double bal = economy.getBalance(p);
                        inform(p, "You have " + economy.format(bal));
                    }
                }

                // DELETE  ********************************************************
            } else if (cmd(p, args, "delete", "D")) {
                deleteDungeon(p, args[1]);

                // RESET  ********************************************************
            } else if (cmd(p, args, "reset")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    resetDungeon(p, dung.getName());
                    inform(p, "Reset " + dung.getName());
                }
            } else if (cmd(p, args, "reset", "D")) {
                resetDungeon(p, args[1]);
                inform(p, "Reset " + args[1]);

                // SUSPEND  ********************************************************
            } else if (cmd(p, args, "suspend")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    suspendDungeon(p, dung.getName());
                    inform(p, "Suspend " + dung.getName());
                }
            } else if (cmd(p, args, "suspend", "D")) {
                suspendDungeon(p, args[1]);
                inform(p, "Suspend " + args[1]);

                // ENABLE  ********************************************************
            } else if (cmd(p, args, "enable")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    enableDungeon(p, dung.getName());
                    inform(p, "Enable " + dung.getName());
                }
            } else if (cmd(p, args, "enable", "D")) {
                enableDungeon(p, args[1]);
                inform(p, "Enable " + args[1]);

                // TIME  ********************************************************
            } else if (cmd(p, args, "time", "t")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    setResetMinMax(p, dung.getName(), args[1]);
                    inform(p, dung.getName() + " reset in " + args[1]);
                }
            } else if (cmd(p, args, "time", "Dt")) {
                setResetMinMax(p, args[1], args[2]);
                inform(p, args[1] + " reset in " + args[2]);

                // WHEN  ********************************************************
            } else if (cmd(p, args, "when")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    inform(p, dung.getName() + " reset time=" + dung.getResetTime());
                }
            } else if (cmd(p, args, "when", "D")) {
                Dungeon dung = dungeons.get(args[1]);
                if (dung != null) {
                    inform(p, args[1] + " reset time=" + dung.getResetTime());
                }
                // STYLE  ********************************************************
            } else if (cmd(p, args, "style")) {
                inform(p, "Dungeon style=" + cnf.getStyle());
            } else if (cmd(p, args, "style", "s")) {
                cnf.setStyle(args[1]);
                inform(p, "Dungeon style=" + cnf.getStyle());

                // RESETALL ********************************************************
            } else if (cmd(p, args, "resetall")) {
                for (String name : dungeons.getNames()) {
                    inform(p, "Reseting dungeon '" + name + "'");
                    resetDungeon(p, name);
                }

                // UNPROT  ********************************************************
            } else if (cmd(p, args, "unprot", "D")) {
                unprotDungeon(p, args[1]);

                // GOTO  ********************************************************
            } else if (cmd(p, args, "goto")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    gotoDungeon(p, dung.getName());
                    inform(p, "Goto " + dung.getName());
                }
            } else if (cmd(p, args, "goto", "D")) {
                gotoDungeon(p, args[1]);
                inform(p, "Goto " + args[1]);

                // END  ********************************************************
            } else if (cmd(p, args, "end")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    gotoDungeonEnd(p, dung.getName());
                    inform(p, "Goto end " + dung.getName());
                }
            } else if (cmd(p, args, "end", "D")) {
                gotoDungeonEnd(p, args[1]);
                inform(p, "Goto end " + args[1]);

                // IRON  ********************************************************
            } else if (cmd(p, args, "ironall")) {
                int cnt = dungeons.changeDoorsToIron();
                inform(p, "Converted " + cnt + " dungeons to have iron doors");
            } else if (cmd(p, args, "iron")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    dung.changeDoorsToIron();
                    inform(p, dung.getName() + " now has iron doors");
                }
            } else if (cmd(p, args, "iron", "D")) {
                Dungeon dung = dungeons.get(args[1]);
                dung.changeDoorsToIron();
                inform(p, args[1] + " now has iron doors");

                // RECALL ********************************************************
            } else if (cmd(p, args, "recall")) {
                Dungeon dung = getDungeon(p);
                if (dung != null) {
                    Location ploc = p.getLocation();
                    Location eloc = dung.getBotLocation();
                    if (ploc != null && eloc != null) {
                        double dist = ploc.distance(eloc);
                        if (dist <= 4) {
                            gotoDungeon(p, dung.getName());
                        } else {
                            inform(p, "'" + dung.getName() + "' too far from the final chest");
                        }
                    }
                }

                // WHICH
            } else if (cmd(p, args, "which") || cmd(p, args, "?")) {
                Dungeon dung = getDungeon(p);
                if (dung != null)
                    inform(p, "Dungeon '" + dung.getName() + "'");

                // RELOAD
            } else if (cmd(p, args, "reload")) {
                cnf = new CatConfig(getConfig());
                inform(p, "config reloaded");

                // LOAD
            } else if (cmd(p, args, "load", "s")) {
                int cnt = loadWorld(args[1]);
                inform(p, cnt + " dungeon(s) loaded/reloaded in " + args[1]);

                // TEST
            } else if (cmd(p, args, "test")) {
            } else if (cmd(p, args, "debug")) {
            } else {
                help(p);
            }
        } catch (IllegalAccessException e) {
            inform(p, e);
        } catch (Exception e) {
            inform(p, e);
        }
        return true;
    }

    @SuppressWarnings("deprecation")
    public Dungeon getDungeon(Player p) {
        if (p == null) {
            inform(p, "Need to specify the dungeon by name in the console");
        } else {
            Dungeon dung = dungeons.which(p.getLocation().getBlock());
            if (dung != null)
                return dung;
            dung = dungeons.which(p.getTargetBlock(null, 1000));
            if (dung != null)
                return dung;
            inform(p, "Not in a dungeon (or not looking at one)");
        }
        return null;
    }

    public void help(Player p) {
        inform(p, "/cat scatter <name> <#levels> <radius> <distance>");
        inform(p, "/cat unplan  <name>");
        inform(p, "/cat delete  <name>");
        inform(p, "/cat unprot  <name>");
        inform(p, "/cat suspend [<name>]");
        inform(p, "/cat enable  [<name>]");
        inform(p, "/cat goto    [<name>]");
        inform(p, "/cat time    [<name>] <time>");
        inform(p, "/cat end     [<name>]");
        inform(p, "/cat reset   [<name>]");
        inform(p, "/cat resetall");
        inform(p, "/cat recall");
        inform(p, "/cat ?");
        inform(p, "/cat style [<style_name>]");
        inform(p, "/cat list");
        inform(p, "/cat gold");
        inform(p, "/cat plan    <name> <#levels> [<radius>]");
        inform(p, "/cat build   <name>");
    }

    private Boolean cmd(Player player, String[] args, String s) throws IllegalAccessException, Exception {
        return cmd(player, args, s, "");
    }

    private Boolean cmd(Player player, String[] args, String s, String arg_types) throws IllegalAccessException, Exception {
        if (args[0].equalsIgnoreCase(s)) {
            if (!hasPermission(player, "catacombs." + s)) {
                throw new IllegalAccessException("You don't have permission for /cat " + s);
            }
            if (args.length != arg_types.length() + 1) {
                return false;
            }
            for (int c = 0; c < arg_types.length(); c++) {
                String arg = args[c + 1];
                char t = arg_types.charAt(c);
                if (t == 'D') {           // Built Dungeon
                    if (!dungeons.isBuilt(arg)) {
                        throw new Exception("'" + arg + "' is not a built dungeon");
                    }
                } else if (t == 'u') {    // Planned Dungeon (ready to be build)
                    if (!dungeons.exists(arg)) {
                        throw new Exception("'" + arg + "' is not a planned dungeon");
                    }
                    if (dungeons.isBuilt(arg)) {
                        throw new Exception("'" + arg + "' has already been built");
                    }
                } else if (t == 'p') {    // Planned Dungeon (ready to be build)
                    if (!dungeons.exists(arg)) {
                        throw new Exception("'" + arg + "' is not a planned dungeon");
                    }
                    if (dungeons.isBuilt(arg)) {
                        throw new Exception("'" + arg + "' has already been built");
                    }
                    if (!dungeons.isOk(arg)) {
                        throw new Exception("'" + arg + "' had issue during planning");
                    }
                } else if (t == 'd') {    // Non existing dungeon
                    if (dungeons.isBuilt(arg)) {
                        throw new Exception("'" + arg + "' is already built");
                    }
                } else if (t == 's') {    // A string

                } else if (t == 't') {    // A time
                    if (!arg.matches("^(\\d+[smhd])+$") && !arg.matches("^(\\d+[smhd])+-(\\d+[smhd])+$")) {
                        throw new Exception("Expecting arg#" + (c + 1) + " to be days/hours/mins/secs (e.g 10m5s) (found '" + arg + "')");
                    }
                } else if (t == 'n') {     // A number
                    try {
                        Integer.parseInt(arg);
                    } catch (NumberFormatException e) {
                        throw new Exception("Expecting arg#" + (c + 1) + " to be a number (found '" + arg + "')");
                    }
                }
            }
            return true;
        }
        return false;
    }

    public void planDungeon(Player p, String dname, int depth, int radius) {
        int tmp_radius = cnf.RadiusMax();
        cnf.setRadiusMax(radius);
        planDungeon(p, dname, depth);
        cnf.setRadiusMax(tmp_radius);
    }

    public void planDungeon(Player p, String dname, int depth) {
        if (p == null) {
            inform(p, "You can't plan from the console");
            return;
        }
        Location loc = p.getLocation();
        Block blk = loc.getBlock();
        Dungeon dung = new Dungeon(this, dname, p.getWorld());
        dung.prospect(p, blk.getX(), blk.getY(), blk.getZ(), getCardinalDirection(p), depth);
        dung.show();
        dungeons.add(dname, dung);
        inform(p, dung.summary());
        if (dung.isOk()) {
            Dungeon overlap = dungeons.getOverlap(dung);
            if (overlap != null) {
                inform(p, "'" + dname + "' overlaps dungeon '" + overlap.getName() + "' (replan or remove it)");
            } else if (!dung.isNatural()) {
                inform(p, "Loaction of '" + dname + "' is no longer solid-natural (replan)");
            } else {
                inform(p, "'" + dname + "' is good and ready to be built");
                // dung.saveMap(mapdir + File.separator + dname + ".map");
            }
        } else {
            inform(p, "'" + dname + "' is incomplete (usually too small for final room/stair)");
        }
    }

    public void scatterDungeon(Player p, String dname, int depth, int radius, int distance) {
        if (p == null) {
            inform(p, "Can't scatter from console at the moment (will need to supply world name)");
            return;
        }
        Block from = p.getLocation().getBlock();
        scatterDungeon(from, p, dname, depth, radius, distance);
    }

    private void scatterDungeon(Block from, Player p, String dname, int depth, int radius, int distance) {
        World world = from.getWorld();

        int x, z;
        Block safe_blk = null;
        Dungeon dung = new Dungeon(this, dname, world);
        int att1 = 0;  // Outer loop attempts
        do {
            int att2 = 0;  // Inner loop attempts
            do {  // Attempts to find a natural surface block to start from
                x = from.getX() + cnf.nextInt(distance * 2) - distance;
                z = from.getZ() + cnf.nextInt(distance * 2) - distance;
                Location loc = world.getBlockAt(x, from.getY(), z).getLocation();
                safe_blk = world.getHighestBlockAt(loc).getLocation().getBlock();
                safe_blk = safe_blk.getRelative(BlockFace.DOWN);
                if (!cnf.isNatural(safe_blk))
                    safe_blk = null;
                att2++;
            } while (safe_blk == null && att2 < 20);

            if (safe_blk != null) { // Now see if it's a good dungeon location
                Direction dir = Direction.any(cnf.rnd);
                int mx = safe_blk.getX() + dir.dx(3);
                int my = safe_blk.getY() + 1;
                int mz = safe_blk.getZ() + dir.dy(3);

                int tmp_radius = cnf.RadiusMax();
                cnf.setRadiusMax(radius);
                dung.prospect(p, mx, my, mz, dir, depth);
                cnf.setRadiusMax(tmp_radius);
                if (dung.isOk()) {
                    dung.show();
                    dungeons.add(dname, dung);
                    buildDungeon(p, dname);
                    inform(p, dung.summary());
                    inform(p, "'" + dname + "' has been built");
                }
            }
            att1++;

        } while (!dung.isOk() && att1 < 30);  // Try another location if it's no good


        if (!dung.isOk()) {
            inform(p, "Failed to find a good location for dungeon '" + dname + "'");
        }
    }

    public void buildDungeon(Player p, String dname) {
        if (dungeons.exists(dname)) {
                Dungeon dung = dungeons.get(dname);
                if (dung.isBuilt()) {
                    inform(p, "Dungeon " + dname + " has already been built");
                } else {
                    inform(p, "Building " + dname);
                    dung.setupFlagsLocations();
                    dung.saveDB();
                    dung.render(handler);
                    dung.saveMap(mapdir + File.separator + dname + ".map");
                    handler.add(p);
                }
        } else {
            inform(p, "Dungeon " + dname + " doesn't exist");
        }
    }

    public void suspendDungeon(Player p, String dname) {
        dungeons.suspend(dname);
    }

    public void gotoDungeon(Player p, String dname) {
        Dungeon dung = dungeons.get(dname);
        if (!dung.teleportToTop(p)) inform(p, "Can't teleport to start of this dungeon");
    }

    public void gotoDungeonEnd(Player p, String dname) {
        Dungeon dung = dungeons.get(dname);
        if (!dung.teleportToBot(p)) inform(p, "Can't teleport to end of a dungeon without any levels");
    }

    public void setResetMinMax(Player p, String dname, String t) {
        Dungeon dung = dungeons.get(dname);
        if (dung != null) dung.setResetMinMax(t);
    }

    public void enableDungeon(Player p, String dname) {
        dungeons.enable(dname);
    }

    public void deleteDungeon(Player p, String dname) {
        Dungeon dung = dungeons.get(dname);
        dung.unrender(this, handler);
        handler.add(p);
        dungeons.remove(dname);
    }

    public void resetDungeon(Player p, String dname) {
        Dungeon dung = dungeons.get(dname);
        dung.reset();
    }

    public void unprotDungeon(Player p, String dname) {
        dungeons.remove(dname);
    }

    public int loadWorld(String name) {
        return dungeons.loadWorld(name);
    }

    public void unloadWorld(String name) {
        dungeons.unloadWorld(name);
    }

    public void inform(Player p, Exception e) {
        String msg = e.getMessage();
        if (msg == null) {
            inform(p, "Exception trapped " + e);
            e.printStackTrace();
        } else {
            inform(p, msg);
        }
    }

    public void inform(Player p, String msg) {
        if (msg == null) {
            msg = "";
        }
        if (p == null)
            System.out.println("[" + info.getName() + "] " + msg);
        else
            p.sendMessage(msg);
    }

    public void inform(Player p, List<String> msgs) {
        for (String msg : msgs) {
            inform(p, msg);
        }
    }

    public static Direction getCardinalDirection(Player p) {
        float y = p.getLocation().getYaw();
        if (y < 0) {
            y += 360;
        }
        y %= 360;
        return getDirection(y);
    }

    private static Direction getDirection(double rot) {
        if (0 <= rot && rot < 45.0) {
            return Direction.SOUTH;
        } else if (45.0 <= rot && rot < 135.0) {
            return Direction.WEST;
        } else if (135.0 <= rot && rot < 225.0) {
            return Direction.NORTH;
        } else if (225.0 <= rot && rot < 315.0) {
            return Direction.EAST;
        } else if (315.0 <= rot && rot < 360.0) {
            return Direction.SOUTH;
        } else {
            return null;
        }
    }
}
