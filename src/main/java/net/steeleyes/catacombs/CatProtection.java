package net.steeleyes.catacombs;

import org.bukkit.World;
import org.bukkit.block.Block;

interface CatProtection {
    Boolean isProtected(Block blk);

    Boolean isProtected(World world, int x, int y, int z);

    void addCube(World world, CatCuboid cube);

    void removeCube(World world, CatCuboid cube);
}
