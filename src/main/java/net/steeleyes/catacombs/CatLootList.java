package net.steeleyes.catacombs;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class CatLootList {
    private final List<String> list = new ArrayList<>();
    private String name;

    public CatLootList(FileConfiguration fcnf, String name, String path) {
        this.name = name;
        List<String> l = fcnf.getStringList(path);
        if (l != null) for (String str : l) list.add(str);
    }

    @Override
    public String toString() {
        return name + " " + list;
    }

    public void add(String str) {
        list.add(str);
    }
}
