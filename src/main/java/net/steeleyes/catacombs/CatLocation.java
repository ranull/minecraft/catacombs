package net.steeleyes.catacombs;

import net.steeleyes.data.CatSQL;
import org.bukkit.util.Vector;

import java.sql.ResultSet;

public class CatLocation {
    private Vector loc;
    private int xid = -1;

    public CatLocation(Vector loc) {
        this.loc = loc;
    }

    public CatLocation(int x, int y, int z) {
        this(new Vector(x, y, z));
    }

    public CatLocation(ResultSet rs) throws Exception {
        this(rs.getInt("x"), rs.getInt("y"), rs.getInt("z"));
        xid = rs.getInt("xid");
    }

    @Override
    public String toString() {
        return "END_CHEST " + xid + " " + loc;
    }

    public void saveDB(CatSQL sql, int did) {
        if (xid <= 0) {
            sql.command("INSERT INTO locations " +
                    "(did,type,x,y,z) VALUES (" + did + ",'END_CHEST'," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + ");");
            xid = sql.getLastId();
        } else System.err.println("[Catacombs] INTERNAL ERROR: CatLocation .db updates not implemented yet");
    }
}

