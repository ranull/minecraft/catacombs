package net.steeleyes.catacombs;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.Dispenser;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Openable;
import org.bukkit.block.data.type.Bed;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CatCuboid extends Cuboid {
    private Type type = Type.LEVEL;
    private World world = null;

    public enum Type {LEVEL, HUT}

    public CatCuboid(World world, int xl, int yl, int zl, int xh, int yh, int zh, Type type) {
        super(xl, yl, zl, xh, yh, zh);
        this.type = type;
        this.world = world;
    }

    public CatCuboid(World world, int xl, int yl, int zl, int xh, int yh, int zh) {
        super(xl, yl, zl, xh, yh, zh);
        this.world = world;
    }

    @Override
    public String toString() {
        return super.toString() + " world=" + world.getName() + " type=" + type;
    }

    public List<String> dump(Vector top) {
        List<String> info = map();
        info.add(" ");
        for (int y = yl; y <= yh; y++)
            for (int x = xl; x <= xh; x++)
                for (int z = zl; z <= zh; z++) {
                    Block blk = world.getBlockAt(x, y, z);
                    info.add((x - top.getX()) + "," + (y - top.getY()) + "," + (z - top.getZ()) + "," + blk.getType());
                }
        return info;
    }

    public void setType(Type t) {
        type = t;
    }

    public Type getType() {
        return type;
    }

    public int guessRoofSize() {
        int depth = 256;
        for (int x = xl; x <= xh; x++)
            for (int z = zl; z <= zh; z++)
                for (int y = yh, i = 0; y >= yl; y--, i++)
                    if (world.getBlockAt(x, y, z).getType() == Material.AIR) {
                        if (i == 0) break;          // Stop if top is AIR
                        if (i < depth) depth = i;
                    }
        if (depth == 256) depth = 1;
        return depth;
    }

    public int guessRoomSize() {
        int max_depth = 0;
        for (int x = xl; x <= xh; x++)
            for (int z = zl; z <= zh; z++) {
                int depth = 0;
                for (int y = yh, i = 0; y >= yl; y--, i++)
                    if (world.getBlockAt(x, y, z).getType() == Material.AIR) {
                        if (i == 0) break;          // Stop if top is AIR
                        depth++;
                    } else if (depth > 0) {
                        if (depth > max_depth) max_depth = depth;
                        break;          // Stop on first solid after air.
                    }
            }
        if (max_depth < 3) max_depth = 3;
        return max_depth;
    }

    public Vector guessEndLocation() {
        // Look for a ladder down first
        for (int x = xl; x <= xh; x++)
            for (int z = zl; z <= zh; z++)
                if (world.getBlockAt(x, yl, z).getType() == Material.LADDER) return new Vector(x, yl - 1, z);
        // If no ladder down then look for the end chest
        for (int x = xl; x <= xh; x++)
            for (int z = zl; z <= zh; z++)
                for (int y = yl; y <= yh; y++)
                    if (isBigChest(world.getBlockAt(x, y, z))) return new Vector(x, yl - 1, z);
        return null;
    }

    static boolean debug = false;

    // TODO: Badly need to make the major/minor block names persist!!
    //   Hunt for a frequent soild block just below the roof level that's
    //   next to air/web/torch. Call that the major material
    public CatMat guessMajorMat(int roofDepth) {
        List<BlockFace> dirs = new ArrayList<>(Arrays.asList(
                BlockFace.NORTH, BlockFace.EAST,
                BlockFace.SOUTH, BlockFace.WEST));
        final CatMat last = new CatMat(Material.AIR);
        final CatMat best_mat = new CatMat(Material.AIR);
        final CatMat mat = new CatMat(Material.AIR);
        int best = 0;
        int cnt = 0;
        if (debug) System.out.println("[Catacombs] roofDepth=" + roofDepth);

        for (int x = xl; x <= xh; x++)
            for (int z = zl; z <= zh; z++) {
                int y = yh - roofDepth;
                Block blk = world.getBlockAt(x, y, z);
                mat.getBlock(blk);
                if (debug) System.out.println("[Catacombs] Block mat=" + mat + " blk=" + blk.getType());

                if (!mat.is(Material.AIR) && !mat.is(Material.TORCH)) {
                    if (debug)
                        System.out.println("[Catacombs]   soid block Block mat=" + mat + " blk=" + blk.getType());
                    Boolean near_air = false;
                    for (BlockFace dir : dirs) {
                        Material near = blk.getRelative(dir).getType();
                        if (near == Material.AIR || near == Material.TORCH || near == Material.COBWEB) {
                            near_air = true;
                            break;
                        }
                    }
                    if (near_air) {
                        if (debug)
                            System.out.println("[Catacombs]     near air Block mat=" + mat + " blk=" + blk.getType());
                        if (!mat.equals(last)) {
                            if (cnt > best) {
                                best_mat.get(last);
                                best = cnt;
                            }
                            last.get(mat);
                            cnt = 1;
                        } else cnt++;
                    }
                }
            }
        if (cnt > best) {
            best_mat.get(last);
            best = cnt;
        }
        if (debug) System.out.println("[Catacombs] final guess mat = " + best_mat + " best=" + best);
        //debug++;
        return best_mat;
    }

    public Boolean isLevel() {
        return type == Type.LEVEL;
    }

    public Boolean isHut() {
        return type == Type.HUT;
    }

    public Boolean overlaps(CatCuboid that) {
        // higher levels check the worlds match so no need to check again
        assert (world.equals(that.world));
        return intersects(that);
    }

    public Boolean isInRaw(Block blk) {
        // higher levels check the worlds match so no need to check again
        assert (world.equals(blk.getWorld()));
        return super.isIn(blk.getX(), blk.getY(), blk.getZ());
    }

    public void unrender(BlockChangeHandler handler, Boolean emptyChest, int num_air) {
        if (type == Type.LEVEL)
            setCube(handler, Material.STONE, true);
        else {
            CatCuboid floor = new CatCuboid(world, xl, yl, zl, xh, yh - num_air, zh);
            // Go one higher to cover anything (torches typically) on the hut roof.
            CatCuboid upper = new CatCuboid(world, xl, yh - num_air + 1, zl, xh, yh + 1, zh);
            upper.setCube(handler, Material.AIR, emptyChest);
            floor.setCube(handler, Material.STONE, true);
        }
    }

    public void removeTorchs() {
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    if (blk.getType().equals(Material.TORCH) || blk.getType().equals(Material.WALL_TORCH)) {
                        blk.setType(Material.AIR);
                    }
                }
            }
        }
    }

    public void restoreCake() {
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    if (blk.getType().equals(Material.OAK_FENCE)) {
                        Block above = blk.getRelative(BlockFace.UP);
                        if (above.getType().equals(Material.AIR) || above.getType().equals(Material.CAKE)) {
                            above.setType(Material.CAKE);
                        }
                    }
                }
            }
        }
    }

    public void addGlow(CatMat mat, int roofDepth) {
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                int y = yh - roofDepth + 1;
                Block blk = world.getBlockAt(x, y, z);
                Block other = blk.getRelative(BlockFace.DOWN, 1);
                if (mat.equals(blk) && other.getType().equals(Material.AIR)) {
                    blk.setType(Material.GLOWSTONE);
                }
            }
        }
    }

    public void removeGlow(CatMat mat, int roofDepth) {
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                int y = yh - roofDepth + 1;
                Block blk = world.getBlockAt(x, y, z);
                Block other = blk.getRelative(BlockFace.DOWN, 1);
                if (blk.getType().equals(Material.GLOWSTONE) && other.getType().equals(Material.AIR)) {
                    mat.setBlock(blk);
                }
            }
        }
    }

    public void buildWindows(CatMat mat, int height) {
        if (type == Type.HUT) {
            return;
        }

        int x, z;
        int y = yl + height;

        for (z = zl + 1, x = xl; z <= zh - 1; z++)
            if (world.getBlockAt(x - 1, y, z).getType() == Material.AIR &&
                    world.getBlockAt(x + 1, y, z).getType() == Material.AIR) mat.setBlock(world.getBlockAt(x, y, z));
        for (z = zl + 1, x = xh; z <= zh - 1; z++)
            if (world.getBlockAt(x - 1, y, z).getType() == Material.AIR &&
                    world.getBlockAt(x + 1, y, z).getType() == Material.AIR) mat.setBlock(world.getBlockAt(x, y, z));
        for (x = xl + 1, z = zl; x <= xh - 1; x++)
            if (world.getBlockAt(x, y, z - 1).getType() == Material.AIR &&
                    world.getBlockAt(x, y, z + 1).getType() == Material.AIR) mat.setBlock(world.getBlockAt(x, y, z));
        for (x = xl + 1, z = zh; x <= xh - 1; x++)
            if (world.getBlockAt(x, y, z - 1).getType() == Material.AIR &&
                    world.getBlockAt(x, y, z + 1).getType() == Material.AIR) mat.setBlock(world.getBlockAt(x, y, z));
    }

    // TODO CLOSE DOOR
    public void closeDoors() {
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    if (blk.getType().equals(Material.OAK_DOOR) || blk.getType().equals(Material.IRON_DOOR)
                        || blk.getType().equals(Material.OAK_TRAPDOOR)) {
                        Bukkit.getServer().broadcastMessage("door");
                        Openable door = (Openable) blk.getBlockData();
                        door.setOpen(false);
                        blk.setBlockData(door);
                    }
                    // TODO SECRET DOORS
                    /*
                    if (blk.getType().equals(Material.STICKY_PISTON)) {
                        Block power = blk.getRelative(BlockFace.DOWN, 1);
                        if (power instanceof Lightable) {
                            Lightable redstone = (Lightable) power;
                            if (!redstone.isLit()) {
                                redstone.setLit(false);
                                power.setBlockData(redstone);
                                Block upper_door = blk.getRelative(BlockFace.UP, 3);
                                if (upper_door instanceof Door) {
                                    Openable door = (Openable) upper_door.getState();
                                    door.setOpen(false);
                                    upper_door.setBlockData(door);
                                }
                            }
                        }
                    }
                     */
                }
            }
        }
    }

    public void refillChests(CatConfig config) {
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    if (blk.getType().equals(Material.CHEST)) {
                        if (type == Type.LEVEL) {  // Leave chests in the hut
                            Chest chest = (Chest) blk.getState();
                            chest.getInventory().clear(); // Clear and refill chests inside
                            if (isBigChest(blk)) {
                                CatLoot.bigChest(config, chest.getInventory());
                            } else if (isMidChest(blk)) {
                                CatLoot.midChest(config, chest.getInventory());
                            } else {
                                CatLoot.smallChest(config, chest.getInventory());
                            }
                        }
                    }
                    if (blk.getType().equals(Material.DISPENSER)) {
                        Dispenser cont = (Dispenser) blk.getState();
                        cont.getInventory().clear(); // Clear and refill chests inside
                        CatLoot.fillChest(cont.getInventory(), config.TrapList());
                        cont.getInventory().addItem(new ItemStack(Material.ARROW, 4));
                        cont.update();
                    }
                }
            }
        }
    }

    public void restoreCobWebs(CatConfig cnf) {
        removeCobWebs();
        replaceCobWebs(cnf);
    }

    public void removeCobWebs() {
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    if (blk.getType().equals(Material.COBWEB)) {
                        blk.setType(Material.AIR);
                    }
                }
            }
        }
    }

    public void replaceCobWebs(CatConfig cnf) {
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    if (blk.getType().equals(Material.AIR)) {
                        blk.setType(cnf.AirType());
                    }
                }
            }
        }
    }

    public void clearMonsters() {
        for (Entity ent : world.getEntities()) {
            if (ent instanceof Creature) {
                Location loc = ent.getLocation();
                Block blk = loc.getBlock();
                if (isIn(blk.getX(), blk.getY(), blk.getZ())) {
                    ent.remove();
                }
            }
        }
    }

    // Issues with the secret door piston creation in earlier versions of Catacombs
    //   left some secret doors broken (only opening by 1 block rather than 2).
    //   This code finds any problem locations by inspection of nearby blocks
    //   and fixes them.
    // TODO FIX SECRET DOOR
    public int fixSecretDoors() {
        int cnt = 0;
        for (int x = xl; x <= xh; x++)
            for (int z = zl; z <= zh; z++)
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    Block above = blk.getRelative(BlockFace.UP);
                    Block above2 = above.getRelative(BlockFace.UP);
                    Block below = blk.getRelative(BlockFace.DOWN);
                    if (blk.getType() == Material.STICKY_PISTON && below.getType() == Material.REDSTONE_TORCH && above.getType() != Material.PISTON_HEAD) {
                        //System.out.println("[Catacombs] Fixing closed secret door "+blk+" "+mat+" "+above.getType()+" "+below.getType());
                        above.setType(Material.PISTON_HEAD);
                        blk.setType(Material.STICKY_PISTON);
                        cnt++;
                    } else if (blk.getType() == Material.STICKY_PISTON &&
                            below.getType() != Material.REDSTONE_TORCH && above2.getType() != Material.AIR) {
                        //System.out.println("[Catacombs] fixing open secret door "+blk+" "+mat+" "+above.getType()+" "+below.getType());
                        above.setType(above2.getType());
                        blk.setType(Material.STICKY_PISTON);
                        above2.setType(Material.AIR);
                        cnt++;
                    }
                }
        return cnt;
    }

    // Minecraft v1.2.3 Changed the way door hinges are positioned
    //   This code will fix up the old dungeons by inspection of nearby
    //   blocks. It will also close all the doors.
    public int fixDoors() {
        int cnt = 0;
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    Material mat = blk.getType();
                    Block above = blk.getRelative(BlockFace.UP);
                    if ((mat.equals(Material.OAK_DOOR) && above.getType().equals(Material.OAK_DOOR)) ||
                            (mat.equals(Material.IRON_DOOR) && above.getType().equals(Material.IRON_DOOR))) {
                        // TODO FIX DOORS
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }

    public int changeDoorsToIron() {
        int cnt = 0;
        for (int x = xl; x <= xh; x++) {
            for (int z = zl; z <= zh; z++) {
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    Material mat = blk.getType();
                    Block above = blk.getRelative(BlockFace.UP);
                    if (mat.equals(Material.OAK_DOOR) && above.getType().equals(Material.IRON_DOOR)) {
                        blk.setType(Material.IRON_DOOR);
                        // TODO CHANGE DOORS TO IRON
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }

    public static Boolean isBigChest(Block blk) {
        if (blk.getType() != Material.CHEST)
            return false;

        int cnt = 0;
        Material major = Material.COBBLESTONE;
        Material minor = Material.MOSSY_COBBLESTONE;

        Material below = blk.getRelative(BlockFace.DOWN, 1).getType();

        for (BlockFace dir : Arrays.asList(
                BlockFace.NORTH, BlockFace.EAST,
                BlockFace.SOUTH, BlockFace.WEST)) {
            Block b = blk.getRelative(dir, 1);
            if (b.getType() == major || b.getType() == minor) {
                cnt++;
            }
        }

        return below == Material.GRASS || cnt == 3;
    }

    public static Boolean isMidChest(Block blk) {
        int cnt = 0;

        Block b;
        b = blk.getRelative(BlockFace.EAST, 1);
        if (b.getType() == Material.CHEST) cnt++;
        b = blk.getRelative(BlockFace.NORTH, 1);
        if (b.getType() == Material.CHEST) cnt++;
        return cnt == 1;
    }

    public void setCube(BlockChangeHandler handler, Material mat, Boolean emptyChest) {
        for (int x = xl; x <= xh; x++)
            for (int z = zl; z <= zh; z++)
                for (int y = yl; y <= yh; y++) {
                    Block blk = world.getBlockAt(x, y, z);
                    Object o = blk.getState();
                    if (emptyChest && o != null && o instanceof InventoryHolder)
                        ((InventoryHolder) o).getInventory().clear();
                    Material before = blk.getType();
                    if (before.equals(Material.LADDER) ||
                            before.equals(Material.TORCH) ||
                            before.equals(Material.OAK_TRAPDOOR) ||
                            before.equals(Material.WATER) ||
                            before.equals(Material.LAVA) ||
                            before.equals(Material.RED_MUSHROOM) ||
                            before.equals(Material.BROWN_MUSHROOM) ||
                            before.equals(Material.REDSTONE_TORCH) ||
                            before.equals(Material.IRON_DOOR) ||
                            before.equals(Material.OAK_WALL_SIGN) ||
                            before.equals(Material.OAK_SIGN) ||
                            before.equals(Material.VINE) ||
                            before.equals(Material.OAK_DOOR)) {
                        handler.add(blk, mat, Position.HIGH);
                    } else if (before.equals(Material.RED_BED)) {
                        BlockData md = blk.getBlockData();
                        if (md instanceof Bed) {
                            Bed bed = (Bed) md;
                            if (bed.getPart().equals(Bed.Part.HEAD)) {
                                Block foot = blk.getRelative(bed.getFacing().getOppositeFace());
                                foot.setType(Material.AIR);
                                blk.setType(Material.AIR);
                                handler.add(blk, mat, Position.LOW);
                                handler.add(blk, mat, Position.LOW);
                            }
                        }
                    } else if (o != null && o instanceof InventoryHolder) {
                        blk.setType(Material.AIR);
                        blk.breakNaturally();
                        handler.add(blk, mat, Position.LOW);
                    } else handler.add(blk, mat, Position.LOW);
                }
    }

    public Boolean isNatural(CatConfig cnf) {
        for (int x = xl; x <= xh; x++) {
            for (int y = yl; y <= yh; y++) {
                for (int z = zl; z <= zh; z++) {
                    if (!cnf.isNatural(world.getBlockAt(x, y, z))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    // Try and figure out an approximate map from the world blocks
    public List<String> map() {
        List<String> info = new ArrayList<>();
        char[][] grid = new char[xh - xl + 1][zh - zl + 1];
        for (int x = xl; x <= xh; x++) for (int z = zl; z <= zh; z++) grid[x - xl][z - zl] = ' ';
        for (int y = yl; y <= yh; y++) {
            for (int x = xl; x <= xh; x++) {
                for (int z = zl; z <= zh; z++) {
                    Block blk = world.getBlockAt(x, y, z);
                    char was = grid[x - xl][z - zl];
                    if (blk.getType().equals(Material.PISTON)) grid[x - xl][z - zl] = '$';
                    if ((was == ' ' || was == ',') && (blk.getType().equals(Material.COBBLESTONE) || blk.getType().equals(Material.MOSSY_COBBLESTONE) || blk.getType().equals(Material.STONE_BRICKS) || blk.getType().equals(Material.SANDSTONE)))
                        grid[x - xl][z - zl] = '#';
                    if ((was == '#' || was == ' ') && (blk.getType().equals(Material.AIR))) grid[x - xl][z - zl] = ',';
                    if ((was == ',') && (blk.getType().equals(Material.AIR))) grid[x - xl][z - zl] = '.';
                    if (blk.getType().equals(Material.GLASS) || blk.getType().equals(Material.GLASS_PANE))
                        grid[x - xl][z - zl] = 'G';
                    if (blk.getType().equals(Material.WATER)) grid[x - xl][z - zl] = 'W';
                    if (blk.getType().equals(Material.LAVA)) grid[x - xl][z - zl] = 'L';
                    if (blk.getType().equals(Material.RED_BED)) grid[x - xl][z - zl] = 'z';
                    if (blk.getType().equals(Material.COBWEB)) grid[x - xl][z - zl] = 'w';
                    if (blk.getType().equals(Material.IRON_BLOCK)) grid[x - xl][z - zl] = 'A';
                    if (blk.getType().equals(Material.TORCH)) grid[x - xl][z - zl] = 't';
                    if (blk.getType().equals(Material.SPAWNER)) grid[x - xl][z - zl] = 'M';
                    if (blk.getType().equals(Material.CHEST)) grid[x - xl][z - zl] = 'c';
                    if (blk.getType().equals(Material.CRAFTING_TABLE)) grid[x - xl][z - zl] = 'T';
                    if (blk.getType().equals(Material.FURNACE)) grid[x - xl][z - zl] = 'f';
                    if (blk.getType().equals(Material.OAK_DOOR)) grid[x - xl][z - zl] = '+';
                    if (blk.getType().equals(Material.LADDER)) grid[x - xl][z - zl] = '^';
                    if (blk.getType().equals(Material.STONE_PRESSURE_PLATE)) grid[x - xl][z - zl] = 'x';
                    if (blk.getType().equals(Material.SOUL_SAND)) grid[x - xl][z - zl] = 's';
                    if (blk.getType().equals(Material.CAKE)) grid[x - xl][z - zl] = '=';
                    if (blk.getType().equals(Material.OAK_TRAPDOOR)) grid[x - xl][z - zl] = 'v';
                }
            }
        }
        for (int z = zl; z <= zh; z++) {
            String s = "# ";
            for (int x = xl; x <= xh; x++) {
                char ch = grid[x - xl][z - zl];
                s += (ch == ',') ? '#' : ch;
            }
            info.add(s);
        }
        info.add(" ");
        return info;
    }
}
