package net.steeleyes.catacombs;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.*;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.type.Door;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class BlockChangeHandler implements Runnable {
    private int changed = 0;

    private Catacombs plugin;

    private final List<BlockChange> delay = new ArrayList<>();
    private final List<BlockChange> high = new ArrayList<>();
    private final List<BlockChange> low = new ArrayList<>();
    private final List<Player> who = new ArrayList<>();

    public BlockChangeHandler(Catacombs plugin) {
        this.plugin = plugin;
    }

    public BlockFace getSideFace(BlockFace front) {
        if (front.equals(BlockFace.NORTH)) {
            return BlockFace.WEST;
        }
        if (front.equals(BlockFace.WEST)) {
            return BlockFace.SOUTH;
        }
        if (front.equals(BlockFace.SOUTH)) {
            return BlockFace.EAST;
        }
        if (front.equals(BlockFace.EAST)) {
            return BlockFace.NORTH;
        }
        return null;
    }

    public BlockFace getMatBesideFace(Block blk, Material mat) {
        if (blk.getRelative(BlockFace.NORTH).getType().equals(mat)) {
            return BlockFace.NORTH;
        }
        if (blk.getRelative(BlockFace.SOUTH).getType().equals(mat)) {
            return BlockFace.SOUTH;
        }
        if (blk.getRelative(BlockFace.EAST).getType().equals(mat)) {
            return BlockFace.EAST;
        }
        if (blk.getRelative(BlockFace.WEST).getType().equals(mat)) {
            return BlockFace.WEST;
        }
        return null;
    }

    private void setBlock(BlockChange x) {
        Block blk = x.getBlk();

        try {
            blk.setType(x.getMat());
        } catch (Exception e) {
            System.err.println("[Catacombs] Problem setting block " + blk + " " + x.getMat());
        }

        // Directional
        if (blk.getBlockData() instanceof Directional) {
            Directional dir = (Directional) blk.getBlockData();
            BlockFace face = x.getFace();
            if (face != null) {
                dir.setFacing(face);
                blk.setBlockData(dir);
            }
        }

        // If door
        if (x.getBlk() != null && blk.getBlockData() instanceof Door) {
            Block doorBottom = blk;
            Block door = doorBottom.getRelative(BlockFace.UP);
            BlockData blockData = Bukkit.getServer().createBlockData(Material.OAK_DOOR);
            Bisected bisected = (Bisected) blockData;

            bisected.setHalf(Bisected.Half.BOTTOM);
            doorBottom.setBlockData(bisected, false);

            bisected.setHalf(Bisected.Half.TOP);
            BlockFace beside = getMatBesideFace(x.getBlk(), Material.OAK_DOOR);
            if (beside != null) {
                if (beside.equals(BlockFace.EAST) || beside.equals(BlockFace.WEST)) {
                    ((Door) blockData).setHinge(Door.Hinge.RIGHT);
                }
                if (beside.equals(BlockFace.NORTH) || beside.equals(BlockFace.SOUTH)) {
                    ((Door) blockData).setHinge(Door.Hinge.RIGHT);
                    ((Door) door.getRelative(beside).getBlockData()).setHinge(Door.Hinge.LEFT);
                }
            }
            door.setBlockData(bisected, false);
        }

        // If inventory
        if (blk.getState() instanceof InventoryHolder) {
            InventoryHolder cont = (InventoryHolder) blk.getState();
            Inventory inv = cont.getInventory();

            // If chest
            if (x.getMat().equals(Material.CHEST)) {
                // Join chests
                BlockFace beside = getMatBesideFace(x.getBlk(), Material.CHEST);
                if (beside != null) {
                    blk.setBlockData(Material.CHEST.createBlockData("[facing=" + getSideFace(beside).toString().toLowerCase() + ",type=left]"));
                    blk.getRelative(beside).setBlockData(Material.CHEST.createBlockData("[facing=" + getSideFace(beside).toString().toLowerCase() + ",type=right]"));
                }
                // Spawn loot
                if (CatCuboid.isBigChest(blk)) {
                    CatLoot.bigChest(plugin.getCnf(), inv);
                } else if (CatCuboid.isMidChest(blk)) {
                    CatLoot.midChest(plugin.getCnf(), inv);
                } else {
                    CatLoot.smallChest(plugin.getCnf(), inv);
                }
            }
            // If dispenser
            if (x.getMat().equals(Material.DISPENSER)) {
                inv.addItem(new ItemStack(Material.ARROW,new Random().nextInt((12 - 3) + 1) + 3));
            }
        }

        // If spawner
        if (x.getSpawner() != null && blk.getState() instanceof CreatureSpawner) {
            CreatureSpawner spawner = (CreatureSpawner) blk.getState();
            spawner.setSpawnedType(x.getSpawner());
            spawner.update(true);
        }

        // If sign
        if (x.hasLines() && blk.getState() instanceof Sign) {
            Sign sign = (Sign) blk.getState();
            for (int i = 0; i < 4; i++) {
                String str = x.getLine(i);
                if (str != null) {
                    sign.setLine(i, str);
                }
            }
            sign.update(true);
        }
    }

    @Override
    public void run() {
        int maxChanges = plugin.getCnf().getMaxChangesPerSecond();
        while (!delay.isEmpty()) {
            BlockChange x = delay.remove(0);
        }

        int cnt = 0;
        while (!high.isEmpty() && cnt < maxChanges) {
            BlockChange x = high.remove(0);
            setBlock(x);
            cnt++;
        }
        if (cnt == 0) // Only do low priority on a new tick
            while (!low.isEmpty() && cnt < maxChanges) {
                BlockChange x = low.remove(0);
                setBlock(x);
                cnt++;
            }
        if (cnt > 0) {
            changed += cnt;
        }
        if (cnt == 0 && changed > 0) {
            System.out.println("[Catacombs] Block Handler #changes=" + changed);
            for (Player p : who) {
                if (plugin != null) {
                    plugin.inform(p, "Catacomb changes complete");
                }
            }
            who.clear();
            changed = 0;
        }
    }

    // figure out a good way to avoid the long list of combinations here
    public void add(BlockChange b, Position pos) {
        if (pos == Position.LOW) {
            this.low.add(b);
        } else {
            this.high.add(b);
        }
    }

    public void add(Block blk, Material mat, Position pos) {
        add(new BlockChange(blk, mat), pos);
    }

    public void add(World world, int x, int y, int z, Material mat, BlockFace face, Position pos) {
        add(new BlockChange(world.getBlockAt(x, y, z), mat, face), pos);
    }

    public void add(World world, int x, int y, int z, Material mat, Position pos) {
        add(new BlockChange(world.getBlockAt(x, y, z), mat), pos);
    }

    public void add(Player player) {
        who.add(player);
    }
}