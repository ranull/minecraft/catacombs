package net.steeleyes.catacombs;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

import java.util.List;

public class MobType {
    private int hps;
    private String name;
    private MobShape shape;
    private List<CatLootList> loot;

    public MobType(String name, String shape, int hps, List<CatLootList> loot) {
        this.name = name;
        this.shape = CatUtils.getEnumFromString(MobShape.class, shape);
        this.hps = hps;
        this.loot = loot;
    }

    public LivingEntity spawn(Location loc) {
        return shape.spawn(loc.getWorld(), loc);
    }

    public int getHps() {
        return hps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MobShape getShape() {
        return shape;
    }
}
