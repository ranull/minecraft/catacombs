package net.steeleyes.data;

import java.util.Arrays;

public enum ECatConfig {
    emptyChest("Admin.emptyChestonDelete", true),
    SecretDoorOnlyInDungeon("Admin.SecretDoorOnlyInDungeon", true),
    GoldOff("Admin.GoldOff", false),
    ProtectSpawners("Admin.ProtectSpawners", true),
    NoPvPInDungeon("Admin.NoPvPInDungeon", true),
    RespawnInHut("Admin.RespawnInHut", false),
    MobsSpawnOnlyUnderground("Admin.MobsSpawnOnlyUnderground", false),
    MobsSpawnOnlyInDungeons("Admin.MobsSpawnOnlyInDungeons", false),
    NoArmourInDungeon("Admin.NoArmourInDungeon", false),
    MobDropReductionPct("Admin.MobDropReductionPct", 100),
    NoTeleportIn("Admin.NoTeleportIn", true),
    NoTeleportOut("Admin.NoTeleportOut", true),
    //BossEnabled             ("Admin.BossEnabled", false),
    Economy("Admin.Economy", "any"),
    AdvancedCombat("Admin.AdvancedCombat", false),
    GroupRadius("Admin.Group.Radius", 50),
    GroupDepth("Admin.Group.Depth", 40),
    GroupHpFactor("Admin.Group.HpFactor", 0.5),
    ClickIronDoor("Admin.ClickIronDoor", false),
    SpawnRadius("Admin.Spawn.Radius", 20),
    SpawnDepth("Admin.Spawn.Depth", 8),
    MonsterRadius("Admin.Spawn.Monster.Radius", 15),
    MonsterMax("Admin.Spawn.Monster.Max", 3),
    DeathGearCost("Admin.Death.GearCost", 25),
    DeathKeepGear("Admin.Death.KeepGear", true),
    DeathExpKept("Admin.Death.ExpKept", 0.85),
    DoorFixDone("Admin.Patches.DoorFixDone", false),
    SecretDoorFixDone("Admin.Patches.SecretDoorFixDone", false),
    BannedCommands("Admin.BannedCommands", Arrays.asList(
            "/spawn",
            "/kill",
            "/warp",
            "/setpwarp",
            "/setwarpp",
            "/setwarp",
            "/warp create",
            "/warp pcreate",
            "/warp createp",
            "/home",
            "/sethome",
            "/homeset",
            "/home set",
            "/tp",
            "/tphere",
            "/tpa",
            "/tpahere",
            "/tspawn",
            "/t spawn",
            "/town spawn",
            "/plot claim",
            "/tclaim",
            "/town claim"
    )),
    BreakBlocks("Admin.Breakable", Arrays.asList(
            "TORCH",
            "WALL_TORCH",
            "RED_MUSHROOM",
            "BROWN_MUSHROOM",
            "COBWEB"
    )),
    PlaceBlocks("Admin.Placeable", Arrays.asList(
            "TORCH",
            "WALL_TORCH"
    )),
    RadiusMax(".RadiusMax", 12),
    UnderFill(".UnderFill", true),
    OverFill(".OverFill", true),
    ResetButton(".ResetButton", false),
    RecallButton(".RecallButton", true),
    HutType(".Hut.Type", "default"),
    majorBlock(".Block.Major", "COBBLESTONE"),
    minorBlock(".Block.Minor", "MOSSY_COBBLESTONE"),
    floorBlock(".Block.Floor", "COBBLESTONE"),
    roofBlock(".Block.Roof", "COBBLESTONE"),
    IronDoorPct(".Archway.IronDoorPct", 0),
    floorDepth(".Depth.floor", 4),
    roomDepth(".Depth.room", 4),
    roofDepth(".Depth.roof", 2),
    extraDepth(".Depth.firstLevel", 2),
    MossyPct(".Block.MossyPct", 2),
    AirWebPct(".Block.AirWebPct", 10),
    NaturalBlocks(".Block.Natural", Arrays.asList(  // Most common blocks first for efficiency
            "STONE",
            "GRANITE",
            "DIORITE",
            "ANDESITE",
            "GRASS",
            "DIRT",
            "GRASS_BLOCK",
            "SAND",
            "SANDSTONE",
            "GRAVEL",
            "COAL_ORE",
            "IRON_ORE",
            "REDSTONE_ORE",
            "GOLD_ORE",
            "DIAMOND_ORE",
            "LAPIS_ORE",
            "EMERALD_ORE",
            "AIR",
            "CLAY",
            "WATER",
            "LAVA",
            "ICE",
            "BROWN_MUSHROOM",
            "RED_MUSHROOM",
            "CHEST",
            "RAIL",
            "COBWEB",
            "OAK_FENCE",
            "OAK_PLANKS",
            "OAK_LOG",
            "OAK_LEAVES",
            "BIRCH_FENCE",
            "BIRCH_PLANKS",
            "BIRCH_LOG",
            "BIRCH_LEAVES",
            "TORCH",
            "COBWEB",
            "SPAWNER",
            "OBSIDIAN",
            "SEAGRASS",
            "KELP"
    )),
    TrapList(".Trap.Ammo", Arrays.asList(  // Most common blocks first for efficiency
            "arrow:100:10"
    )),

    SpiderPct(".Mob.Type.SpiderPct", 5),
    SkeletonPct(".Mob.Type.SkeletonPct", 25),
    WolfPct(".Mob.Type.WolfPct", 7),
    PigmanPct(".Mob.Type.PigmanPct", 8),
    CaveSpiderPct(".Mob.Type.CaveSpiderPct", 10),
    BlazePct(".Mob.Type.BlazePct", 0),
    CreeperPct(".Mob.Type.CreeperPct", 5),
    EndermanPct(".Mob.Type.EndermanPct", 0),
    SlimePct(".Mob.Type.SlimePct", 0),
    // SilverFishPct(".Mob.Type.SilverFishPct",5),
    GoldMin(".Mob.Gold.Min", 1),
    GoldMax(".Mob.Gold.Max", 10),

    SmallEquipPct(".Loot.Small.LeatherEquipPct", 10),
    MedSweepOre(".Loot.Medium.SweepOre", true),
    MedEquipPct(".Loot.Medium.EquipPct", 100),
    MedSmallPct(".Loot.Medium.SmallPct", 100),
    BigEquipPct(".Loot.Big.EquipPct", 100),
    BigSmallPct(".Loot.Big.SmallPct", 100),

    LootSmallList(".Loot.Small.List", Arrays.asList(
            "LEATHER:10:1-6",
            "TORCH:50:1-30",
            "WHEAT:10:1-5",
            "GOLD_INGOT:10:1-5",
            "REDSTONE:5:1-4",
            "GLOWSTONE_DUST:15:1-6",
            "SLIME_BALL:7:1",
            "IRON_INGOT:20:1-4",
            "ARROW:10:1-25",
            "GUNPOWDER:10:1-5",
            "PUMPKIN:0:5:1",
            "FLINT:10:1-6",
            "MUSIC_DISC_13:2:1",
            "MUSIC_DISC_CAT:2:1",
            "SADDLE:2:1",
            "DIAMOND:1:1",
            "MOSSY_COBBLESTONE:5:1-12",
            "OBSIDIAN:2:1-8",
            "GOLDEN_APPLE:2:1",
            "COOKIE:4:8",
            "BREAD:3:4",
            "APPLE:3:4",
            "COD:3:4",
            "COOKED_BEEF:3:4",
            "COOKED_CHICKEN:3:4",
            "COOKED_PORKCHOP:3:4",
            "MELON_SEEDS:2:1",
            "PUMPKIN_SEEDS:2:1",
            "BOWL:7:1",
            "WHEAT_SEEDS:4:1-6",
            "BOOK:7:1-4",
            "PAPER:7:1-4",
            "COMPASS:5:1",
            "CLOCK:5:1",
            "PAINTING:5:1"
    )),

    LootMediumList(".Loot.Medium.List", Arrays.asList(
            "LEATHER:10:1-6",
            "TORCH:50:1-30",
            "WHEAT:10:1-5",
            "GOLD_INGOT:10:1-5"
    )),

    LootBigList(".Loot.Big.List", Arrays.asList(
            "DIAMOND:100:1-3",
            "GOLDEN_APPLE:30:1"
    )),

    maxBlockChangesPerSecond("Max Block Changes Per Second", 1000);

    private String str;
    private Object def;

    ECatConfig(String str, Object def) {
        this.str = str;
        this.def = def;
    }

    public String getStr() {
        return str;
    }

    public Object getDef() {
        return def;
    }
}