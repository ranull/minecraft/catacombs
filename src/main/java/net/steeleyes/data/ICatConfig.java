package net.steeleyes.data;

import net.steeleyes.catacombs.CatMat;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;

import java.util.List;

public interface ICatConfig extends IConfig {
    Integer RadiusMax();

    Integer floorDepth();

    Integer roomDepth();

    Integer roofDepth();

    Integer extraDepth();

    String HutType();

    Boolean UnderFill();

    Boolean OverFill();

    Boolean ResetButton();

    Boolean emptyChest();

    Boolean ProtectSpawners();

    Boolean SecretDoorOnlyInDungeon();

    Boolean GoldOff();

    Boolean RespawnInHut();

    Boolean NoPvPInDungeon();

    Boolean NoArmourInDungeon();

    Boolean MobsSpawnOnlyUnderground();

    Boolean MobsSpawnOnlyInDungeons();

    Boolean AdvancedCombat();

    Integer GroupRadius();

    Integer GroupDepth();

    Double GroupHpFactor();

    Integer SpawnRadius();

    Integer SpawnDepth();

    Integer MonsterRadius();

    Integer MonsterMax();

    Integer DeathGearCost();

    Boolean DeathKeepGear();

    Double DeathExpKept();

    Boolean ClickIronDoor();

    // Boolean BossEnabled();

    String Economy();

    List<String> BannedCommands();

    EntityType SpawnerType();

    Double Gold();

    Material ShroomType();

    Material AirType();

    Material DoorMaterial();

    Boolean NoTeleportIn();

    Boolean NoTeleportOut();

    Boolean isNatural(Block b);

    Boolean isBreakable(Block b);

    Boolean isPlaceable(Block b);

    List<String> TrapList();

    List<String> LootSmallList();

    List<String> LootMediumList();

    List<String> LootBigList();

    Boolean SmallEquipChance();

    Boolean MobDropReductionChance();

    Boolean MedEquipChance();

    Boolean MedSmallChance();

    Boolean MedSweepOre();

    Boolean BigEquipChance();

    Boolean BigSmallChance();

    Boolean MinorChance();

    CatMat majorMat();

    CatMat minorMat();

    CatMat floorMat();

    CatMat roofMat();

    void setRadiusMax(int r);
}
