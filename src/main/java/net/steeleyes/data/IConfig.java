package net.steeleyes.data;

public interface IConfig {
    Boolean MedHalfEmpty();

    Boolean SandChance();

    Boolean ChestChance();

    Boolean SpawnerChance();

    Boolean EnchantChance();

    Boolean TrapChance();

    Boolean PoolChance();

    Boolean FullPoolChance();

    Boolean SpecialChance();

    Boolean CorridorChance();

    Boolean ShroomChance();

    Boolean BenchChance();

    Boolean AnvilChance();

    Boolean OvenChance();

    Boolean DoubleDoorChance();
}
